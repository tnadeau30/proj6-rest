"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

control_speeds_kmh = {0: (15, 34), 200: (15, 32), 400: (15, 30), 600: (11.428, 28), 1000: (13.333, 26)}
# Different min/max speed for km markers, formatted: {low end of control dist: (min_spd, max_spd), ...}

overall_time_limits = {200: (13,30), 300: (20,00), 400: (27,00), 600: (40,00), 1000: (75,00)}
# Rules time limits for different lengths of brevets. Used for close_time()
# Formmatted: {brevet_dist_km: (hours, minutes)} where hours & minutes is amount of time after start
# where the final control point closes



def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
   """
   start_time_arrow = arrow.get(brevet_start_time)

   time = 0
   max_end_point = brevet_dist_km * 1.2
   if control_dist_km > max_end_point: # Check if control point is toofar beyond brevet dist
      return "Error: Control point is too far out" # need help ehre?
   
   # Make sure that open times stay same for control points
   # beyond the brevet length
   if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

   # Control @ 0km case
   if control_dist_km == 0:
      return start_time_arrow.isoformat()


   # Get the different speed changes they go through
   ramp_ups = [] # And put them in list
   for key in control_speeds_kmh.keys(): 
      if control_dist_km > key:
         ramp_ups.append(key)
         continue
      break
    
    # Calculate the times for all but last ramp
   for i in range(len(ramp_ups[:-1])):
      min_spd, max_spd = control_speeds_kmh[ramp_ups[i]]
      time += (ramp_ups[i+1] - ramp_ups[i]) / max_spd
    
   # Calculate the remainder
   remainder = control_dist_km - ramp_ups[-1]
   min_spd, max_spd = control_speeds_kmh[ramp_ups[-1]]
   time += remainder / max_spd
   
   calcd_hours, calcd_minutes = float_to_time(time)
   start_time_arrow = start_time_arrow.shift(hours=+calcd_hours)
   start_time_arrow = start_time_arrow.shift(minutes=+calcd_minutes)
   return start_time_arrow.isoformat()





def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
   """
   start_time_arrow = arrow.get(brevet_start_time)

   time = 0
   max_end_point = brevet_dist_km * 1.2

   # Control @ 0km closes 1 hour after start
   if control_dist_km == 0:
      start_time_arrow = start_time_arrow.shift(hours=+1)
      return start_time_arrow.isoformat()

   if control_dist_km > max_end_point: # Check if control point is toofar beyond brevet dist
      return "Error: Control point is too far out" # Again, help here?

   if control_dist_km >= brevet_dist_km:
      # END OF BREVET, use time shift of rules
      calcd_hours, calcd_minutes = overall_time_limits[brevet_dist_km]
      start_time_arrow = start_time_arrow.shift(hours=+calcd_hours)
      start_time_arrow = start_time_arrow.shift(minutes=+calcd_minutes)
      return start_time_arrow.isoformat()

   # Get the different speed changes they go through
   ramp_ups = [] # And put them in list
   for key in control_speeds_kmh.keys(): 
      if control_dist_km > key:
         ramp_ups.append(key)
         continue
      break

   # Calculate the times for all but last ramp
   for i in range(len(ramp_ups[:-1])):
      min_spd, max_spd = control_speeds_kmh[ramp_ups[i]]
      time += (ramp_ups[i+1] - ramp_ups[i]) / min_spd
   
   # Calculate the remainder
   remainder = control_dist_km - ramp_ups[-1]
   min_spd, max_spd = control_speeds_kmh[ramp_ups[-1]]
   time += remainder / min_spd
   
   calcd_hours, calcd_minutes = float_to_time(time)
   start_time_arrow = start_time_arrow.shift(hours=+calcd_hours)
   start_time_arrow = start_time_arrow.shift(minutes=+calcd_minutes)

   return start_time_arrow.isoformat()



def float_to_time(num):
    """Returns time a tuple of (hours, minutes)"""
    time = round(num, 3)
    hours = 0
    minutes = 0
    while time > 1:
        hours += 1
        time -= 1
    minutes = round(time * 60)
    if minutes >= 60:
        minutes -= 60
        hours += 1
    return (hours, minutes)