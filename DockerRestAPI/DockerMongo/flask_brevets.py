"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import flask
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#client = MongoClient("db", 27017)
db = client.tododb
###
# Pages
###




@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


#### From app.py ####
# Routes to display


@app.route('/display')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) == 0:
                return render_template("errordisplay.html")


    return render_template('todo.html', brevet=items)

# This adds item to DB, from "SUBMIT" button
@app.route('/new', methods=['POST'])
def new():
    #only prob with this is i need to submit and get all rows of table in
    open_close_times = {
        'distance': request.form['distance'],
        'location': request.form['location'],
        'control_dist': request.form['km'],
        'open': request.form['open'],
        'close': request.form['close']
    }
    points = request.form.getlist('km')
    num_points = 0
    for i in range(len(points)):
        if points[i] != "":
            num_points += 1
    if num_points == 0:
        return render_template('error.html')

    brevet = {
        'distance': request.form['distance'],
        #'control_times': open_close_times,
        'list_control_dist': request.form.getlist('km'),
        'list_open': request.form.getlist('open'),
        'list_close': request.form.getlist('close'),
        'list_location': request.form.getlist('location'),
        'num_points': num_points
        }
    db.tododb.insert_one(open_close_times)

    return redirect(url_for('index'))
    #return redirect(url_for('todo'))


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist_km = request.args.get('brevet_dist_km', 200, type=int)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')

    app.logger.debug("km={}".format(km)) # DEBUG
    app.logger.debug("BREVET_DIST_KM = {}".format(brevet_dist_km)) # DEBUG
    app.logger.debug("begin_date = {}".format(begin_date)) # DEBUG
    app.logger.debug("begin_time = {}".format(begin_time)) # DEBUG
    app.logger.debug("request.args: {}".format(request.args)) # DEBUG

    brevet_start_time = arrow.get(begin_date + " " + begin_time, 'YYYY-MM-DD HH:mm')

    # print(f"brevet_start_time flask.py: {brevet_start_time.isoformat()}") # DEBUG

    open_time = acp_times.open_time(km, brevet_dist_km, brevet_start_time.isoformat()) # Original code bug here with .isoformat not function?
    close_time = acp_times.close_time(km, brevet_dist_km, brevet_start_time.isoformat())
    result = {"open": open_time, "close": close_time}
    # print(f"flask_brevets: Open = {result['open']}") # DEBUG
    # print(f"flask_brevets: Close = {result['close']}") # DEBUG

    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
